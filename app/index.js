'use strict';
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.generators.Base.extend({
  initializing: function () {
    this.pkg = require('../package.json');
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Workflow Component UI Generator'
    ));

    var prompts = [{
      name: 'componentCode',
      message: '请输入工作流组件的code',
      default: '01'
    }];

    this.prompt(prompts, function (props) {
      this.componentCode = props.componentCode;

      done();
    }.bind(this));
  },

  configuring: {
    enforceFolderName: function () {
      var folderName = 'workflow-component-ui-' + this.componentCode;
      if (folderName !== _.last(this.destinationRoot().split(path.sep))) {
        this.destinationRoot(folderName);
      }
      this.config.save();
    }
  },

  writing: {
    app: function () {
      this.fs.copyTpl(
        this.templatePath('_package.json'),
        this.destinationPath('package.json'),
        {code: this.componentCode}
      );
      this.fs.copyTpl(
        this.templatePath('_bower.json'),
        this.destinationPath('bower.json'),
        {code: this.componentCode}
      );
      this.fs.copy(
        this.templatePath('.gitignore'),
        this.destinationPath('.gitignore')
      );
      this.fs.copyTpl(
        this.templatePath('Gruntfile.js'),
        this.destinationPath('Gruntfile.js'),
        {code: this.componentCode}
      );

      this.fs.copyTpl(
        this.templatePath('preview/preview.html'),
        this.destinationPath('preview/preview.html'),
        {code: this.componentCode}
      );
      this.fs.copy(
        this.templatePath('preview/preview.css'),
        this.destinationPath('preview/preview.css')
      );

      this.fs.copyTpl(
        this.templatePath('component/component.html'),
        this.destinationPath('component/component.html'),
        {code: this.componentCode}
      );
      this.fs.copyTpl(
        this.templatePath('component/component.js'),
        this.destinationPath('component/component.js'),
        {code: this.componentCode}
      );
      this.fs.copyTpl(
        this.templatePath('component/component.scss'),
        this.destinationPath('component/component.scss'),
        {code: this.componentCode}
      );
      this.fs.copy(
        this.templatePath('component/modal.html'),
        this.destinationPath('component/modal.html')
      );
    }
  },

  install: function () {
    this.installDependencies({
      skipInstall: this.options['skip-install']
    });
  },

  end: function () {
    this.log('运行grunt编译项目，在浏览器中打开preview/preview.html预览工作流组件设置界面。')
  }
});
