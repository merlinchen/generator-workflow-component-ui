angular.module('c<%= code %>', ['ui.bootstrap', 'ngGrid', 'template-component-<%= code %>'])
  .directive('c<%= code %>', function () {
    return {
      restrict: 'E',
      templateUrl: 'c<%= code %>/component.html',
      replace: true,
      scope: false
    }
  })
  .controller('C<%= code %>Controller', ['$scope', '$modal', function ($scope, $modal) {
    $scope.showModal = function () {
      $modal.open({
        templateUrl: 'c<%= code %>/modal.html',
        backdropClass: 'bootstrap',
        windowClass: 'bootstrap'
      });
    };

    $scope.myData = [
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      },
      {
        tableName: '_MAIN',
        fieldCName: '表单ID',
        fieldName: 'ID',
        allowEmpty: true
      }
    ];

    $scope.gridOptions = {
      data: 'myData',
      columnDefs: [
        {
          field: 'tableName',
          displayName: '表名'
        },
        {
          field: 'fieldCName',
          displayName: '字段名'
        },
        {
          field: 'fieldName',
          displayName: '英文名'
        },
        {
          field: 'allowEmpty',
          displayName: '不允许为空',
          cellTemplate: '<div style="text-align: center;" class="ngCellText"><input type="checkbox" ng-model="row.entity.allowEmpty" ng-click="!row.entity.allowEmpty"></div>'
        }
      ]
    };
  }]);

