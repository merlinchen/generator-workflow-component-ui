module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    html2js: {
      options: {
        base: 'component',
        module: 'template-component-<%= code %>',
        singleModule: true,
        rename: function(moduleName) {
          return 'c<%= code %>/' + moduleName
        }
      },
      dist: {
        src: ['component/**/*.html'],
        dest: 'tmp/templates.js'
      }
    },

    concat: {
      dist: {
        src: ['component/*.js', 'tmp/templates.js'],
        dest: 'dist/component.js'
      }
    },

    compass: {
      dist: {
        options: {
          raw: 'Sass::Script::Number.precision = 10\n',
          sassDir: 'component/',
          cssDir: 'dist',
          environment: 'development',
          require: 'compass/import-once/activate'
        }
      }
    }
  });

  grunt.registerTask('default', ['html2js', 'concat', 'compass']);
};
